# **Documentation de la librairie de scan de documents:**

Cette librairie vous permet de scanner vos documents, vous pouvez capturer ou importer vos images et avoir en résultat le document scanné.

# Ajout de la librairie compressée (.aar file) au projet:

La librairie est ajoutée au projet en tant que librairie externe (.aar).

## **Appel de la librairie :** 

*  La librairie utilise par défaut le style, images, buttons et textes de CARONAE Systems.
Pour les modifier, il suffit de spécifier les paramètres envoyés à la librairie, et en respectant les types supportés: 

   - Couleurs: String représentant le code hexa
   - Texte des boutons: String
   - Images des boutons et logos: byte Array Output Stream

* Pour passer une variable à la librairie il faut spécifier son ID disponible dans la classe Utils de la librairie.

###  Description des IDs des variables: 

| ID                        | Description                                                        |
| ------------------------- | ------------------------------------------------------------------ |
| **IMG_BTN_CAPTURE**       | Icone du bouton de capture image dans l'activité de la caméra.     |
| **IMG_BTN_FLASH**         | Icone du bouton du Flash dans l'activité de la caméra.             |
| **STRING_CONTOUR_COLOR**  | Couleur du contour qui s'affiche au dessus de l'image indiquant le contour du document.                                                                             |
| **IMG_BACK_BUTTON**       | Icone du bouton retour.                                            |
| **STRING_BACK_BUTTON**    | Texte du bouton retour.                                            |
| **IMG_ROTATE_LEFT**       | Icone du bouton rotation à gauche.                                 |
| **IMG_ROTATE_RIGHT**      | Icone du bouton rotation à droite.                                 |
| **IMG_LOAD_FILE**         | Icone du bouton de chargement d'une image depuis la galerie.       |
| **IMG_LOGO_HEADER**       | Icone du logo de la librairie.                                     |
| **BUTTON_COLOR**          | Couleur des boutons de la librairie.                               |
| **STRING_SCAN**           | Texte du bouton scanner/découper le document.                      |
| **STRING_FINISH**         | Texte du bouton terminer.                                          |
| **STRING_RETRY**          | Texte du bouton réessayer.                                         |
| **REQUEST_CODE**          | Code pour identifier l'appel de la librairie, il faut l'importer depuis la classe Utils de la librairie, il nous servira pour la récupération de l'image traité par la librairie.  |

   
### Exemple sous Android:

``` java
public class ScanClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            // create new Intent to start ScanActivity
            Intent intent = new Intent(MainActivity.this, ScanActivity.class);

            // send the images
            // Logo header, height: 60dp
            Bitmap bitmap_logo_header = BitmapFactory.decodeResource(getResources(), R.drawable.logo_cih);
            // Capture image, size : 60 dp
            Bitmap bitmap_btn_capture_image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_camera_icon_60dp);
            // Load file image, size : 36 dp
            Bitmap bitmap_btn_load_file = BitmapFactory.decodeResource(getResources(), R.drawable.ic_load_file_36dp);
            Bitmap bitmap_ic_launcher = BitmapFactory.decodeResource(getResources(), R.drawable.ic_image_capture_60dp);
            byte[] baos_launcher = getBaos(bitmap_ic_launcher);


            // **** Transform resources to  byte Array Output Stream ****
            // logo header
            byte[] baos_logo_header = getBaos(bitmap_logo_header);
            // image for capture picture button
            byte[] baos_btn_capture_image = getBaos(bitmap_btn_capture_image);
            // image for load file button
            byte[] baos_btn_load_file = getBaos(bitmap_btn_load_file);

            // hexa code for the color of buttons
            String st_btn_color = "#6bf442";
            // text for scan button
            String st_scan_button = "Scan";
            // text for finish button
            String st_finish_button = "Finish";
            // text for retry button
            String st_retry = "Retry";

            // **** sending variables to Scan activity, they have to match their IDs ****
            // img capture image
            intent.putExtra(Utils.IMG_BTN_CAPTURE, baos_btn_capture_image);
            // img flash button
            intent.putExtra(Utils.IMG_BTN_FLASH, baos_launcher);
            // string flash button
            intent.putExtra(Utils.STRING_CONTOUR_COLOR, "#6bf442");
            // img back button
            intent.putExtra(Utils.IMG_BACK_BUTTON, baos_launcher);
            // string back button
            intent.putExtra(Utils.STRING_BACK_BUTTON, "Back");
            // img rotate left button
            intent.putExtra(Utils.IMG_ROTATE_LEFT, baos_launcher);
            // img rotate right button
            intent.putExtra(Utils.IMG_ROTATE_RIGHT, baos_launcher);
            // img load file from storage
            intent.putExtra(Utils.IMG_LOAD_FILE, baos_btn_load_file);
            // img logo header
            intent.putExtra(Utils.IMG_LOGO_HEADER, baos_logo_header);
            // img button color
            intent.putExtra(Utils.BUTTON_COLOR, st_btn_color);
            // string scan button
            intent.putExtra(Utils.STRING_SCAN, st_scan_button);
            // string finish button
            intent.putExtra(Utils.STRING_FINISH, st_finish_button);
            // string retry button
            intent.putExtra(Utils.STRING_RETRY, st_retry);
            
            // **** start the scan activity ****
            startActivityForResult(intent, REQUEST_CODE);
        }
    }

    /**
     * get byte Array Output Stream
     * @param bitmap
     * @return
     */
    public byte[] getBaos(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }
```

##  **Récupération du résultat du scan:**

  * Après avoir validé et traité l'image, quand l'utilisateur clique sur le bouton terminer, le résultat de l'image scannée est transmis à l'activité qui a fait appel à la librairie.
  * Pour cela, il faut ajouter la fonction onActivityResult dans la même activité:  

### Exemple sous Android: 

``` java
//Once the scanning is done, the application is returned from scan library to main app, to retrieve the scanned image,
    // add onActivityResult in your activity or fragment from where you have started startActivityForResult,
    // below is the sample code snippet:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // here we get the unified resource dientifier of the scanned image
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                // here we get our scanned bitmap
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }  
```

# Réduction de la taille de l'APK: 

Jusqu'à présent nous avons notre code fonctionnel, mais la taille de notre APK sera supérieur à 20MB, ce qui n'encouragera pas les gens à utiliser notre application. 

## Solution: 

Il faudra effectuer un filtre sur les ABI (Application Binary Interface), on otulisera que ("armeabi-v7a", "x86").

### Exemple de filtre pour Android (gradle)

``` java
defaultConfig {
        applicationId "sdk.caronae.scan.demo"
        minSdkVersion 16
        targetSdkVersion 26
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"

        ndk {
            abiFilters "armeabi-v7a", "x86"
        }

    }
```

Et voila, notre apk sera réduit d'environ 10MB. 

Pour plus d'informations vous pouvez consulter notre application de démo: https://gitlab.com/lamraniothmane1/Scan_library_demoApp