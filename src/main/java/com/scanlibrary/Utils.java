package com.scanlibrary;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.ByteBuffer;

import static android.content.ContentValues.TAG;

/**
 * Created by Lamrani on 18/01/2018.
 */
public class Utils {

    private Utils() {

    }

    public final static String IMG_BTN_CAPTURE = "image_btn_capture_camera";
    public final static String IMG_LOGO_HEADER = "image_logo_header";
    public final static String IMG_LOAD_FILE = "image_btn_load_file";
    public final static String BUTTON_COLOR = "button_color";
    public final static String STRING_SCAN = "string_scan";
    public final static String STRING_FINISH = "string_finish";
    public final static String STRING_RETRY = "string_retry";
    public final static String STRING_FLASH = "string_flash";
    public final static String IMG_BTN_FLASH = "image_btn_flash";
    public final static String IMG_ROTATE_RIGHT = "image_btn_rotate_right";
    public final static String IMG_ROTATE_LEFT = "image_btn_rotate_left";
    public final static String IMG_BACK_BUTTON = "image_back_button";
    public final static String STRING_BACK_BUTTON = "string_btn_back";
    public final static String STRING_CONTOUR_COLOR = "contour_color";

    public static final int REQUEST_CODE = 99;

    public static Uri getUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        //bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap getBitmap(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        return bitmap;
    }

    /**
     * Encode to base 64
     */
    public static String encodeTobase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);

        return imageEncoded;
    }

    public static Drawable getDrawable(Context context, String resource_name){
        try{
            int resId = context.getResources().getIdentifier(resource_name, "drawable", context.getPackageName());
            if(resId != 0){
                return context.getResources().getDrawable(resId);
            }
        }catch(Exception e){
            Log.e(TAG,"getDrawable - resource_name: "+resource_name);
            e.printStackTrace();
        }

        return null;
    }

  /*  public static String encodeTobase64(Bitmap image) {
        int bytes = image.getByteCount();
        //or we can calculate bytes this way. Use a different value than 4 if you don't use 32bit images.
        //int bytes = b.getWidth()*b.getHeight()*4;

        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        image.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

        byte[] array = buffer.array();

        String imageEncoded = Base64.encodeToString(array, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);

        return imageEncoded;
    }*/



}