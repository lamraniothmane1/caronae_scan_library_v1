package com.scanlibrary;

import android.net.Uri;

/**
 * Created by Lamrani on 18/01/2018.
 */
public interface IScanner {

    // when a bitmap is selected (from camera or gallery)
    void onBitmapSelect(Uri uri);

    // when the contour detection (scan) is finished
    void onScanFinish(Uri uri);
}
